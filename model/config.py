

# model embedding paramters
max_features=200000
max_senten_len=40
max_senten_num=5
embed_size=100
valiation_split = 0.2
REG_PARAM = 1e-13

# model preprocessing parameters
advanced_preprocess = True
VISUALIZE_DATA = False
DATA_LIMIT = None # If not specified all the data would be loaded or None

# data paths
GLOVE_DIR = "./data/glove.6B.100d.txt"
DATA_PATH = "./data/notebook_data_News_Category_Dataset_v2.json"
DATASET_NAME = 'News_Category'

# visualization data path
STOP_WORDS_DF_PATH = './data/preprocessed/preprocessed_News_Category_with_stop_wordsNone.json' 
CLEAN_DF_PATH = './data/preprocessed/preprocessed_News_CategoryNone.json'
UNPROCESSED_PATH = "/media/lachonman/NewVolume/Studia/programs/MachineLearning_Tutorials/Lazy_My_Programs/large_files/news-category-dataset/News_Category_Dataset.json"

# What part of words is not enceded by Glove vectors:
# basic preprocessing = 32.6% (acc on epoch 1 ~ 0.16)
# current text normalization = 27.58 (acc on epoch 1 ~ 0.16)