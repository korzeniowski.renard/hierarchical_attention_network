from nltk.stem import WordNetLemmatizer #LancasterStemmer
from nltk.corpus import wordnet
from nltk import pos_tag
from tqdm import tqdm
import pandas as pd

'''
Write what is steming and lemmatization

https://nlp.stanford.edu/IR-book/html/htmledition/stemming-and-lemmatization-1.html
https://www.datacamp.com/community/tutorials/stemming-lemmatization-python

'''

class LemmatizationWithPOSTagger():
    '''
        Document this
    '''


    def __init__(self, column):
        # change it to generator mb
        tqdm.pandas(desc="lemmatization")
        self.lemmatizer = WordNetLemmatizer()
        self.lemmatized = column.progress_apply(lambda doc: [self.lemmatize(sentence) for sentence in doc])

    def get_wordnet_pos(self, tag):
        """
        Converts pos taggs to wordnet compatible taggs

        https://stackoverflow.com/questions/15586721/wordnet-lemmatization-and-pos-tagging-in-python
        """
        if tag.startswith('J'):
            return wordnet.ADJ
        elif tag.startswith('V'):
            return wordnet.VERB
        elif tag.startswith('N'):
            return wordnet.NOUN
        elif tag.startswith('R'):
            return wordnet.ADV
        else:
            return ''

    def lemmatize(self, sentence):
        """Lemmatize verbs in list of tokenized words"""
        lemmas = []
        # pos tagging for lemmatizer https://www.nltk.org/book/ch05.html
        tagged_sentence = pos_tag(sentence)
        for word, tag in tagged_sentence:
            valid_tag = self.get_wordnet_pos(tag)
            if valid_tag:
                lemma = self.lemmatizer.lemmatize(word, pos=valid_tag)
            else:
                lemma = word
            lemmas.append(lemma)
        
        return lemmas




# def stem_words(column):
#     """Stem words in list of tokenized words"""
#     stemmer = LancasterStemmer()
#     stems = []
#     for document in column:
#         for sentence in document:
#             for word in sentence:
#                 stem = stemmer.stem(word)
#                 stems.append(stem)
#     return stems