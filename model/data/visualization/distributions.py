import numpy as np
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
from model.data.loader import DataLoader
from nltk import sent_tokenize, word_tokenize
from tqdm import tqdm

from model import config



class DataVisualizer():
    '''
    Plots parameters of the data inside given dataframe

    Parameters
    ----------
    
    df_clean : dataframe with stopwords removed

    df_stop_words : dataframe with stopwords

    df_unpreprocessed : raw dataframe (without normalization nor removed stopwords)

    '''

    '''
    TODO
    Sort by occurances
    Move to functions
    '''

    def __init__(self):
        self.stop_words_df_path = config.STOP_WORDS_DF_PATH
        self.clean_df_path = config.CLEAN_DF_PATH
        self.unprocessed_df_path = config.UNPROCESSED_DF_PATH


    def __call__():
        if config.VISUALIZE_DATA:
        
            loader = DataLoader(UNPROCESSED_PATH, None)
            self.raw_df = loader.data
            self.df_clean = pd.read_json(clean_df_path)
            self.df_with_stop_words = pd.read_json(self.stop_words_df_path)

        
            # plot class distribution
            #self.class_distribution()

            # plot story lenght distribiution
            #self.compare_mean_story_lenght()

            # plot sentence lenght distribution
            self.compare_sentence_lenght_distribution()

            # plot infulence of normalization on number of unique words
            #self.compare_word_count()



    def class_distribution(self):
        # Calculate number of class occurances
        category_count = self.df_clean.groupby('category').count()
        category_count.sort_values(by='text', inplace=True)
        n_cat = len(category_count.index.values)
        y = category_count.values.reshape(n_cat)
        x = np.arange(n_cat)

        # Set window size
        fig, ax = plt.subplots(figsize=(14, 8))
        
        # Set values on x axis to names of the categories verticaly
        my_xticks = category_count.index.values
        plt.xticks(x, my_xticks, rotation='vertical')
        plt.plot(x, y)
        
        # Enable gird
        plt.grid(True)

        # add x and y axis description
        ax.set_title('Category distribution')
        plt.ylabel('Number of class occurance in df')
        plt.xlabel('Categories')        

        # Change proportions of a figure so that vertical category names are visable
        left, bottom, right, top = ax.get_position().bounds
        ax.set_position([left, bottom*2, right, top*0.95])

        plt.show()

    def compare_mean_story_lenght(self):
        '''Comapre story lenght distribution after and before normalization
        (Expected no difference)
        '''
        tqdm.pandas(desc='sentence tokenization')
        raw = self.raw_df['text'].progress_apply(sent_tokenize).apply(len).mean()
        stop_words = self.df_with_stop_words['text'].apply(len).mean()
        clean = self.df_clean['text'].apply(len).mean()
        
        bar_names = ['raw' , 'normalized', 'normalized+stopwords_removed']
        y_pos = np.arange(len(bar_names))
        y = [raw, stop_words, clean]

        plt.bar(y_pos, y, align='center', alpha=0.6)
        plt.xticks(y_pos, bar_names)
        plt.ylabel('Average number of a sentences in a story')
        plt.xlabel('Text preprocessing performed')
        plt.title('Mean lenght of a story')
        plt.ylim(2)

        plt.show()

    def compare_sentence_lenght_distribution(self):
        '''Comapre sentence lenght distribution after and before normalization
        and with and without stopwords removed
        '''
        tqdm.pandas(desc='sentence tokenization')
        raw = self.raw_df['text'].progress_apply(word_tokenize).apply(len)
        stop_words = self.df_with_stop_words['text'].apply(lambda sent: sum([len(x) for x in sent]))
        clean = self.df_clean['text'].apply(lambda sent: sum([len(x) for x in sent]))

        # bar_names = ['raw' , 'normalized', 'normalized+stopwords_removed']
        # y_pos = np.arange(len(bar_names))
        # y = [raw, stop_words, clean]

        # plt.bar(y_pos, y, align='center', alpha=0.6)
        # plt.xticks(y_pos, bar_names)
        # plt.ylabel('ylabel')
        # plt.xlabel('xlabel')
        # plt.title('title')
        # plt.ylim(10)

        # plt.show()

        # create some kind of distribiution plot
        # number of words in a story
        sns.distplot(stop_words, bins=200)
        plt.show()
        sns.distplot(raw, bins=200)
        plt.show()
        sns.distplot(clean, bins=200)
        plt.show()



    def compare_word_count(self):
        '''Comapre number of uniqe words after and before normalization
        and with and without stopwords removed
        '''
        raw_unique_words = set()
        unique_stop_words = set()
        unique_clean = set()

        tqdm.pandas(desc='sentence tokenization')
        raw = self.raw_df['text'].progress_apply(word_tokenize)
        self.df_with_stop_words['text'].apply(lambda story: [unique_stop_words.add(word) for sent in story for word in sent])
        self.df_clean['text'].apply(lambda story: [unique_clean.add(word) for sent in story for word in sent])

        for sentence in raw:
            for word in sentence:
                raw_unique_words.add(word)
        
        # bar_names = ['raw', 'normalized', 'normalized+stopwords_removed']
        # y_pos = np.arange(len(bar_names))
        # y = [len(raw_unique_words), len(unique_stop_words), len(unique_clean)]

        # plt.bar(y_pos, y, align='center', alpha=0.6)
        # plt.xticks(y_pos, bar_names)
        # plt.ylabel('ylabel')
        # plt.xlabel('xlabel')
        # plt.title('title')
        
        # plt.show()
