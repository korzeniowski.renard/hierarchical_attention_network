from model.data.preprocessing.text_preprocessing import DataPreprocessor
from model.data.visualization.distributions import DataVisualizer
from model.model import HAN


from model.data.preprocessing.model_specific_preprocessors.HAN_preprocessor import HAN_preprocessor


# save preprocessed data to a file
# if there is file with preprocessed data (for given size) do nothing else
# preprocess the data

# preprocess data in chunks so it wont crash because of memory issues

def main():
    data_loader = DataPreprocessor()
    df = data_loader()

    distribution_plotter = DataVisualizer()
    distribution_plotter()

    preprocessor = HAN_preprocessor(df)
    embedding_matrix, word_count, category_count = preprocessor.create_embeddings()
    x_train, y_train, x_val, y_val = preprocessor.data_split()

    model = HAN(word_count, category_count, embedding_matrix)
    history = model.fit(x_train, y_train, x_val, y_val)
    model.plot_results(history)
    model.save()

if __name__ == "__main__":
    main()
