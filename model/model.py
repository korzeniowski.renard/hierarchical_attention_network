import numpy as np
import pandas as pd
import tensorflow as tf
import matplotlib.pyplot as plt

from model.custom_layers.Attention import AttentionWithContext
from model import config

#from keras.util import to_categorical
from keras import initializers as initializers, regularizers, constraints
from keras.callbacks import Callback, ModelCheckpoint

from keras.layers import Embedding, Input, Dense, LSTM, GRU, Bidirectional, TimeDistributed, Dropout
from keras import optimizers
from keras.models import Model

import logging

import re
from nltk import tokenize

# split it into classes that make more sense

class HAN:
    def __init__(self, category_count, word_count, embedding_matrix):
        self.texts = []
        self.paras = []
        self.labels = []
        self.sent_lens = []
        self.sent_nums = []
        
        self.l2_reg = regularizers.l2(config.REG_PARAM)
        self.max_senten_len = config.max_senten_len
        self.max_senten_num = config.max_senten_num
        self.embed_size = config.embed_size
        self.category_count = category_count
        self.word_count = word_count

        self.model, self.checkpoint = self.create_model_architecture(embedding_matrix)

    def fit(self, x_train, y_train, x_val, y_val):
        return self.model.fit(x_train, y_train, validation_data=(x_val, y_val), epochs=50, batch_size=512, callbacks=[self.checkpoint])

    def create_model_architecture(self, embedding_matrix):

        embedding_layer = Embedding(self.word_count + 1, self.embed_size, weights=[embedding_matrix], input_length=self.max_senten_len, trainable=False)

        word_input     = Input(shape=(self.max_senten_len,), dtype='float32')
        word_sequences = embedding_layer(word_input)
        word_lstm      = Bidirectional(LSTM(150, return_sequences=True, kernel_regularizer=self.l2_reg))(word_sequences)
        word_dense     = TimeDistributed(Dense(200, kernel_regularizer=self.l2_reg))(word_lstm)
        word_att       = AttentionWithContext()(word_dense)
        word_encoder   = Model(word_input, word_att)

        sent_input     = Input(shape=(self.max_senten_num, self.max_senten_len), dtype='float32')
        sent_encoder   = TimeDistributed(word_encoder)(sent_input) # LSTM returns a sequence
        sent_lstm      = Bidirectional(LSTM(150, return_sequences=True, kernel_regularizer=self.l2_reg))(sent_encoder)
        sent_dense     = TimeDistributed(Dense(200, kernel_regularizer=self.l2_reg))(sent_lstm)
        sent_att       = Dropout(0.5)(AttentionWithContext()(sent_dense))

        preds = Dense(self.category_count, activation='softmax')(sent_att)
        model = Model(sent_input, preds)
        model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['acc'])

        checkpoint = ModelCheckpoint('best_model.h5', verbose=0, monitor='val_loss', save_best_only=True, mode='auto')

        return model, checkpoint

    def plot_results(self, history):

        #plotting
        print(history.history.keys())

        # summarize history for accuracy
        plt.plot(history.history['acc'])
        plt.plot(history.history['val_acc'])
        plt.title('model accuracy')
        plt.ylabel('accuracy')
        plt.xlabel('epoch')
        plt.legend(['train', 'test'], loc='upper left')
        plt.show()


        # summarize history for loss
        plt.plot(history.history['loss'])
        plt.plot(history.history['val_loss'])
        plt.title('model loss')
        plt.ylabel('loss')
        plt.xlabel('epoch')
        plt.legend(['train', 'test'], loc='upper left')
        plt.show()

    def save(self):
        self.model.save('han.h5')









