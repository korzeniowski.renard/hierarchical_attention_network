
from nltk.corpus import stopwords
import re, unicodedata
import inflect
from tqdm import tqdm

# Text normalization
class TextNormalizer():

    def __init__(self, column):
        '''Normalizes each sentence in each row of the dataframe'''
        tqdm.pandas(desc='normalization')
        self.normalized_data = column.progress_apply(lambda doc: [self.normalize_sentence(sentence) for sentence in doc])

    def normalize_sentence(self, words):
        words = self.remove_non_ascii(words)
        words = self.to_lowercase(words)
        words = self.replace_numbers(words)
        words = self.remove_punctuation(words)
        words = self.remove_stopwords(words)
        return words

    def remove_non_ascii(self, words):
        """Remove non-ASCII characters from list of tokenized words"""
        new_words = []
        for word in words:
            new_word = unicodedata.normalize('NFKD', word).encode('ascii', 'ignore').decode('utf-8', 'ignore')
            new_words.append(new_word)
        return new_words

    def to_lowercase(self, words):
        """Convert all characters to lowercase from list of tokenized words"""
        new_words = []
        for word in words:
            new_word = word.lower()
            new_words.append(new_word)
        return new_words

    def remove_punctuation(self, words):
        """Remove punctuation from list of tokenized words"""
        new_words = []
        for word in words:
            new_word = re.sub(r'[^\w\s]', '', word)
            if new_word != '':
                new_words.append(new_word)
        return new_words

    def replace_numbers(self, words):
        """Replace all interger occurrences in list of tokenized words with textual representation"""
        p = inflect.engine()
        new_words = []
        for word in words:
            if word.isdigit():
                new_word = p.number_to_words(word)
                new_words.append(new_word)
            else:
                new_words.append(word)
        return new_words

    def customize_stopwords(self, stopword_list):
        keep_words = ['i', 'you', 'he', 'she', 'it', 'we', 'they']
        remove_words = []

        for word in keep_words:
            if word in stopword_list:
                stopword_list.remove(word)
        
        for word in remove_words:
            if word not in stopword_list:
                stopword_list.append(word)

        return stopword_list

    def remove_stopwords(self, words):
        """Remove stop words from list of tokenized words"""
        new_words = []
        for word in words:
            if word not in stopwords.words('english'):
                new_words.append(word)
        return new_words



