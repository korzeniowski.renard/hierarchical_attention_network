import re
import os
import contractions
import pandas as pd
from tqdm import tqdm
from nltk import sent_tokenize, word_tokenize
from sklearn.utils import shuffle


from model.data.preprocessing.stem_and_lemmatization import LemmatizationWithPOSTagger
from model.data.preprocessing.normalization import TextNormalizer
from model.data.loader import DataLoader
from model import config

class DataPreprocessor():
    '''
    Class tansforms text into form that is usable by model

    If the preprocessed data is already saved in the default location
    data is loaded form the csv instead fo being preprocessed.
    Otherwise given dataframe if preprocessed and preprocessed data is
    save to csv. FUNCTION ASSUMES GIVEN DATAFRAME IS ALWAYS THE SAME


    Paramters
    ---------
    dataset_name : str
        Name of the preprocessed dataset used to name saved preprocessed data

    data_limit : int or str
        Size of preprocessed dataframe. If not specifiled all the data would be loaded

    Atribites
    ---------
    data : pd.DataFrame:
        Preprocessed data in the form of dataframe with columns 'category' and 'text'

    preprocessed_data_path : str
        Path where preprocessed dataset is saved. By default it's
        called 'preprocessed' + <dataset_name> + <data_limit>'.csv'


    '''

    '''
    TODO
    might think about adding some kind of validaiton if given data
    frame is really the same as the one saved on the disk.
    For example by checking number of columnsm, row and adding
    Arguemnt that describes the what part was it preprocessed

    Change progression bar to be on the global level not on the every stage

    '''

    def __init__(self):
        self.data_path = config.DATA_PATH
        self.dataset_name = config.DATASET_NAME
        self.data_limit = config.DATA_LIMIT
        self.preprocessed_data_path = './data/preprocessed/preprocessed_{}{}.json'.format(self.dataset_name, self.data_limit)


    def __call__(self):
        # if self.check_for_preprocessed_data():
        #     print('File for dataset {} with preprocessed dataset found in "{}".\nLoading preprocessed data...'.format(dataset_name, self.preprocessed_data_path))
        #     self.data = pd.read_json(self.preprocessed_data_path)
        #     print('Data Loaded')
        # else:
        #     self.log_stages()
        # loader = DataLoader(data_path, data_limit)
        #    self.data = self.preprocess_text(loader.data)
        #    self.save_preprocessed_data()

        loader = DataLoader(self.data_path, self.data_limit)
        data = loader.load()
        return shuffle(data).reset_index()


    def log_stages(self):
        print('No file with preprocessed dataset in "{}".\nStart preprocessing data...'.format(self.preprocessed_data_path))
        print('Stages:\
        \n-------------\
        \nloading data\
        \replacing contractions\
        \ntokenization\
        \nnormalization\
        \nlemmatization\
        \n-------------')

    def save_preprocessed_data(self):
        self.data.to_json(path_or_buf=self.preprocessed_data_path)

    def check_for_preprocessed_data(self):
        return os.path.isfile(self.preprocessed_data_path)

    def preprocess_text(self, df):
        '''Standardize text to be in the correct for from tokenization'''
        
        df.text = df.text.apply(basic_cleaning)
        df = self.replace_contractions(df)
        df = self.tokenize_text(df)

        if config.advanced_preprocess:
            text_norm = TextNormalizer(df['text'])
            df['text'] = text_norm.normalized_data

            lemmer = LemmatizationWithPOSTagger(df['text'])
            df['text'] = lemmer.lemmatized

        return df

 

    def tokenize_text(self, df):
        # customization of puknt https://nlpforhackers.io/splitting-text-into-sentences/
        # https://machinelearningmastery.com/clean-text-machine-learning-python/
        tqdm.pandas(desc='tokenize doc2sentences 1/2')
        df['text'] = df['text'].progress_apply(sent_tokenize)

        tqdm.pandas(desc='tokenize sentence2words 2/2')
        # split on words. fucks up on stuff like 'word."'
        df['text'] = df['text'].progress_apply(lambda row: [word_tokenize(sentence) for sentence in row])
        return df

    def replace_contractions(self, df):
        '''Replace contractions in string of text
        don't -> dont
        lovin' -> lovin'
        '''
        df = self.remove_citation(df)
        tqdm.pandas(desc='replacing contractions')
        df['text'] = df['text'].progress_apply(contractions.fix)

        return df

    def remove_citation(self, df):
        '''Revmoves single quotation marks that pose a problem
        while replaceing contractions'''
        pat = r"'(?P<word>\w+)'"
        repl = lambda m: m.group('word')
        df['text'] = df['text'].str.replace(pat, repl)
        return df



def basic_cleaning(string):
    """
    Tokenization/string cleaning for dataset
    Every dataset is lower cased except
    """
    string = re.sub(r"\\", "", string)    
    string = re.sub(r"\'", "", string)    
    string = re.sub(r"\"", "", string)    
    return string.strip().lower()     