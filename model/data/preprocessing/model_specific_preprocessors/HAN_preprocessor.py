
import logging
import numpy as np
import pandas as pd
from keras.preprocessing.text import text_to_word_sequence, Tokenizer

from model import config


class HAN_preprocessor:

    def __init__(self, df):
        self.df = df
        self.max_features = config.max_features
        self.max_senten_len = config.max_senten_len
        self.max_senten_num = config.max_senten_num
        self.embed_size = config.embed_size
        self.valiation_split = config.valiation_split


    def create_embeddings(self):
        texts = self.preprocess_data(df)
        self.prepare_data(texts)
        embedding_matrix = self.create_embedding_matrix()

        return embedding_matrix, self.word_count, self.category_count

    def preprocess_data(self, df):
        model_preprocessor = HAN_preprocessor()
        texts = model_preprocessor.create_corpus(df)

        print('Create tokenizer')
        self.tokenizer = self.create_tokenizer(texts)

        return texts

    def prepare_data(self, texts):
        print('Preparing data for training...')
        print('Create data matrix')
        self.data_matrix = np.zeros((len(texts), self.max_senten_num, self.max_senten_len), dtype='int32')
        print('Create word_index_matrix')
        self.word_count = self.word_index_matrix()
        print('Create category labels')
        self.create_category_label()


    def create_embedding_matrix(self):
        # load embeddings
        embeddings_index = {}

        print('Loading embeddings from Glove to dict')

        with open(config.GLOVE_DIR, 'r') as f:
            for line in f:
                try:
                    values = line.split()
                    word = values[0]
                    coefs = np.asarray(values[1:], dtype='float32')
                    embeddings_index[word] = coefs
                except:
                    print('Word not loaded from Glove: {}'.format(word))
                    pass
        
        logging.info('Total {} word vectors'.format(embeddings_index))

        print('Creating embedding matrix...')

        embedding_matrix_exceptions = []

        # create embedding matrix
        embedding_matrix = np.zeros((len(self.word_index) + 1, self.embed_size))
        absent_words = 0
        for word, i in self.word_index.items():
            embeddings_vector = embeddings_index.get(word)
            if embeddings_vector is not None:
                # words not found in embedding index will be all-zeros
                # in other implementaion OOV words had a special token (wasnt 0 and 1 index taken by tokenizer)
                embedding_matrix[i] = embeddings_vector
            else:
                absent_words += 1
                embedding_matrix_exceptions.append(word)
                pass
        
        if embedding_matrix_exceptions:
            print('\tList of words that were not added in word_index_matrix {}'.format(embedding_matrix_exceptions))


        print('Embedding matrix created')

        print('\tTotal absent words are {} which is {} \% of total words'.format(absent_words, absent_words * 100/len(self.word_index)))

        return embedding_matrix


    def create_category_label(self):
        self.labels = pd.get_dummies(self.df.category)

    def create_tokenizer(self, texts):
        tokenizer = Tokenizer(num_words=self.max_features, oov_token=True)
        #texts = self.df['text'].apply(lambda row: [word for sent in row for word in sent])
        tokenizer.fit_on_texts(texts)
        #logging.info('Total {} unique tokens'.format(len(tokenizer.word_index)))

        return tokenizer






        

    def create_corpus(self, df):
        texts = []
        for idx in range(df.text.shape[0]):
            text = clean_str(df.text[idx])
            texts.append(text)
            sentences = tokenize.sent_tokenize(text)
            self.sent_nums.append(len(sentences))
            for sent in sentences:
                self.sent_lens.append(len(text_to_word_sequence(sent)))
            self.paras.append(sentences)

        return texts





    def data_split(self):
        self.category_count = len(self.df.category.unique())
        print('\tThere are 41 categories in the original data. {} are present in dataset'.format(self.category_count))

        indexes = np.arange(self.data_matrix.shape[0])
        np.random.shuffle(indexes)
        self.data_matrix = self.data_matrix[indexes]
        nb_validation_samples = int(self.valiation_split * self.data_matrix.shape[0])

        x_train = self.data_matrix[:-nb_validation_samples]
        y_train = self.labels[:-nb_validation_samples]
        x_val = self.data_matrix[-nb_validation_samples:]
        y_val = self.labels[-nb_validation_samples:]
        logging.info('Number of positive and negative reviews in training and validation set\n{}\n{}\n{}'.format(\
                                                    y_train.columns.tolist(), y_train.sum(axis=0).tolist(), y_val.sum(axis=0).tolist()))
        return x_train, y_train, x_val, y_val

    def word_index_matrix(self):
        '''change word to correct indexes in tockernizer and put them 
        in matrix that has shape [Number of a story, Number of a sentence in story, Number of word in a sentence]
        '''
        index_matrix_exceptions = []
        
        for index, row in enumerate(self.paras):
            #sentences = row.text

            for sent_num, sentence in enumerate(row):
                if sent_num < self.max_senten_num:
                    word_tokens = text_to_word_sequence(sentence)
                    k = 0
                    for word_num, word in enumerate(word_tokens):
                        try:
                            if k < self.max_senten_len and self.tokenizer.word_index[word] < self.max_features:
                                self.data_matrix[index, sent_num, word_num] = self.tokenizer.word_index[word] 
                                k += 1
                        except:
                            index_matrix_exceptions.append(word)
                            #print(word)
                            pass
        
        if index_matrix_exceptions:
            print('\tList of words that were not added in word_index_matrix {}'.format(index_matrix_exceptions))

        self.word_index = self.tokenizer.word_index
        return len(self.word_index)

def clean_str(string):
    """
    Tokenization/string cleaning for dataset
    Every dataset is lower cased except
    """
    string = re.sub(r"\\", "", string)    
    string = re.sub(r"\'", "", string)    
    string = re.sub(r"\"", "", string)    
    return string.strip().lower()