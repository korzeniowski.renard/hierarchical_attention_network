# Implementation of HAN

#### Repository Structure:
	Implementation of HAN
	├── data
    │   ├──preprocessing
    │   │   ├── normalization.py
    │   │   ├── stem_and_lemmatization.py
    │   │   └── text_preprocessing.py
    │   │
    │   ├── visualization
    │   │   └── distributions.py
    │   │
    │   └── loader.py
    │
    ├── model
    │   └── model.py
    │
    ├── resources
    │   └── model_architecture.png
    │
    ├── README.md
    ├── requirements
    └── run_han.py


# Model Architecture:
<p align="center">
  <img src="resources/model_architecture.png"/>
</p>



#### Data
[News Category Dataset](https://www.kaggle.com/rmisra/news-category-dataset/version/1)

###### Preprocessing

Before preprocessing data you need to perform following instructions:
 - Download NLTK resources:
    - Run python
    - In python console run command `import nltk`
    - Download following packages with command `nltk.download(<package_name>)`:
        - 'stopword'
        - 'wordnet'
        - 'averaged_perceptron_tagger'
        - 'punkt'

#### Word Embedding 
[GloVe: Global Vectors for Word Representation](https://nlp.stanford.edu/projects/glove/)

#### TODO
- [x] Create data loader
- [x] Preprocessd data
    - [x] Tokenization of sentences
    - [x] Tokenization of words in sentences
    - [x] Normalization
    - [x] Stem and Lemmatization
    - [x] Saving of preprocessed data
    - [x] Validate if correct data is correctly loaded of preprocessed
    - [x] Move checking if there is a saved data to before loading the whole data
    - [ ] Move apply to the first level so that functions inside work on one row at the time instead of full df
    - [ ] Create documentaion for preprocessing
    - [ ] Think about saving the data into MongoDB
    - [ ] Change serialization to json (lists in csv is not a good idea)
- [ ] Create data visualization
    - [ ] Comperate the size of vocab before and after normalization of the text (and effects of competing techniques)
    - [ ] Class distribution
    - [ ] Sentences distribution
    - [ ] Documents distribution
    - [ ] Save results of visuaization in resource folder
- [ ] Build the model
    - [ ] Create hparams file
    - [ ] Split to train, validation, test sets
    - [ ] Add attention
    - [ ] Saving of the weights
    - [ ] Add prediction
    - [ ] Preprocess input of predict function to correct format
    - [ ] Add support for data from different sources (optional)
    - [ ] Create visualization of attention words in sentences
    - [ ] Create visualization of attention sentences in documents
    - [ ] Implement with different type of attention
    - [ ] Fix class distribiution using e.g. Data agmentation (split prompts into single sentences, mix them and use to make all the classes equinumerous)
    - [ ] [Craete embedding that enables adding and traning new words to fixed pretrained vectors](https://stackoverflow.com/questions/49009386/train-only-some-word-embeddings-keras?fbclid=IwAR1ZhJLzZJVlEL4a_KhCt2MdOvajKvTwtBIwP5UwvfC5FA44B5xM717KD5o)
- [ ] Validate the model
    - [ ] Optimize hyperparamters using e.g. Grid search on Validation set

#### References and Resources:
This model is based on:
- Research paper using this architecture [Hierarchical Attention Networks for Document Classification](http://www.cs.cmu.edu/~./hovy/papers/16HLT-hierarchical-attention-networks.pdf)
- Medium article [Hierarchical Attention Networks](https://medium.com/analytics-vidhya/hierarchical-attention-networks-d220318cf87e)
- Kaggle Notebook [News Classification using HAN](https://www.kaggle.com/hsankesara/news-classification-using-han/notebook)
- Github implementation [Hierarchical Attention Network](https://github.com/Hsankesara/DeepResearch/tree/master/Hierarchical_Attention_Network)
- Text preprocessing tutorial [Text Data Preprocessing: A Walkthrough in Python](https://www.kdnuggets.com/2018/03/text-data-preprocessing-walkthrough-python.html)
- [Check for sth usefull](https://machinelearningmastery.com/prepare-text-data-deep-learning-keras/)