import json
import pandas as pd
from tqdm import tqdm
from model.data.helpers.util import get_num_lines


class DataLoader():
    '''
    Class converts data in the form of stacked json to padnas df

    Paramters
    ---------
    data_path : str
        Absolute path to json with data

    upper_limit : int or None
        How many rows will be loaded from json to dataframe.
        If None all data will be loaded

    Atribites
    ---------
    data_path : str

    columns : list[str]

    data : pd.DataFrame

    '''

    def __init__(self, data_path, upper_limit):
        self.data_path = data_path
        self.columns = ['category', 'text']
        self.upper_limit = upper_limit

    def load(self):
        df = self.json2df(self.data_path, self.upper_limit)
        data = self.preprocess_df(df)
        return data

    def json2df(self, data_path, upper_limit):
        '''
        Converts data in the form of stacked json to padnas df

        Parameters
        ----------
        data_path : str
            Absolute path to data json
        upper_limit : int or None, default value -> None
            How many rows will be loaded from json to dataframe
            Each time items are loaded in the same order from the start
            of the json file. When set to None all data will be loaded

        Returns
        -------
        pd.DataFrame
            Columns as keys in dict
        '''
        if upper_limit:
            # number does not work
            total_lines2read = upper_limit
        else:
            total_lines2read = get_num_lines(data_path)

        data = []
        with open(data_path) as file:
            i = 0
            for line in tqdm(file, total=total_lines2read, desc='loading data'):
                if i == upper_limit:
                    break

                data.append(json.loads(line))
                i+=1

        return pd.DataFrame(data)

    def preprocess_df(self, df):
        '''
        Converts data in the form of stacked json to padnas df

        Parameters
        ----------
        df : pd.DataFrame
            Dataframe that will be processed
        keep_cols : list[str]
            List of columns that will be kept in the returned dataframe

        Returns
        -------
        pd.DataFrame
            Preprocessed dataframe
        '''

        # combine 'headline' and 'short_description' columns into new column called 'text'
        df['text'] = df['headline'] + '. ' + df['short_description']

        # select only important columns
        df = df[self.columns]

        return df


